import { Pipe, PipeTransform } from '@angular/core';
import { EstadosFacturas } from '../facturas/model/estados-facturas.enum';

@Pipe({
  name: 'estadosFacturas'
})
export class EstadosFacturasPipe implements PipeTransform {

  transform(estado: EstadosFacturas): string {
    return EstadosFacturas[estado];
  }

}
