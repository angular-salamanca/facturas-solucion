import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FacturasService } from 'src/app/facturas/facturas.service';

@Component({
  selector: 'fact-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css']
})
export class BuscadorComponent implements OnInit {

  @Output()
  public buscar = new EventEmitter<string>();

  public cadenaBusqueda: string;

  constructor(private facturasService: FacturasService) { }

  ngOnInit() {
  }

  public onBuscar() {
    this.buscar.emit(this.cadenaBusqueda);
  }

  public onNuevaFactura() {
    this.facturasService.anyadirFactura();
  }

}
