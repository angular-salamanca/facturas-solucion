export interface FacturaAPI {
  id: number;
  numero: string;
  fecha: string;
  concepto: string;
  base: number;
  tipoIva: number;
  tipo: string;
  abonada: boolean;
}
