import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'fact-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  @Output('clickSidebar')
  public toggleSidebar = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  public onClickSidebar(e: Event) {
    e.preventDefault();
    this.toggleSidebar.emit();
  }

}
