import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fact-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public cargando = false;
  public sidebarAbierta = false;
  public tituloSeccion = 'Listado de ingresos';
  public txtBusqueda = '';

  constructor() { }

  ngOnInit() { }

  public toggleSidebar() {
    this.sidebarAbierta = !this.sidebarAbierta;
  }

  public buscar(busqueda: string) {
    this.txtBusqueda = busqueda;
  }
}
