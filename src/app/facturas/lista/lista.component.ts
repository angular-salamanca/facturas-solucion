import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { filter } from 'rxjs/operators';
import { FacturasService } from '../facturas.service';
import { Factura } from '../model/factura.interface';
import { Subscription } from 'rxjs';


@Component({
  selector: 'fact-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit, OnDestroy {

  @Input()
  public txtBusqueda;

  public facturas: Factura[];
  public formatoNums = '1.2-2';
  private suscripciones: Subscription[] = [];

  constructor(private facturasService: FacturasService) {
  }

  public onBorrar(e: Event, factura: Factura) {
    e.preventDefault();
    this.facturasService.borrarFactura(factura.id);
  }

  public onCambiarEstado(e: Event, factura: Factura) {
    e.preventDefault();
    this.facturasService.cambiarEstadoFactura(factura);
  }

  ngOnInit() {
    this.suscripciones.push(this.facturasService.facturas$.pipe(
      filter(facturas => facturas.length > 0)
    ).subscribe(
      facturas => this.facturas = facturas
    ));
  }

  ngOnDestroy() {
    this.suscripciones.forEach(
      suscripcion => suscripcion.unsubscribe()
    );
  }
}
