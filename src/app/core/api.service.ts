import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Factura } from '../facturas/model/factura.interface';
import { FacturaAPI } from '../facturas/model/factura-api.interface';
import { map, catchError } from 'rxjs/operators';
import { TiposFacturas } from '../facturas/model/tipos-facturas.enum';
import * as moment from 'moment';
import { EstadosFacturas } from '../facturas/model/estados-facturas.enum';
import { environment } from 'src/environments/environment';
import { FacturasModule } from '../facturas/facturas.module';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private urlApi = 'http://localhost:3000/facturas/';

  constructor(private http: HttpClient) { }

  public getFacturas(): Observable<Factura[]> {
    return this.http.get<FacturaAPI[]>(environment.urlApi).pipe(
      map(
        facturas => this.transformaFacturas(facturas)
      )
    );
  }

  public anyadirFactura(factura: Factura): Observable<Factura> {
    delete (factura.total);
    delete (factura.iva);
    delete (factura.estado);
    const facturaAPI = {
      ...factura,
      tipo: factura.tipo === 1 ? 'gasto' : 'ingreso',
      fecha: '' + moment(factura.fecha).valueOf()
    };
    return this.http.post<FacturaAPI>(environment.urlApi, facturaAPI).pipe(
      map(
        factura => this.transformaFactura(factura)
      )
    );
  }

  public borrarFactura(id: number): Observable<any> {
    return this.http.delete(environment.urlApi + id).pipe(
      catchError(
        err => {
          console.log('Ha habido un pete');
          return throwError(err);
        }
      )
    );
  }

  public cambiarEstadoFactura(factura: Factura): Observable<FacturaAPI> {
    return this.http.patch<FacturaAPI>(environment.urlApi + factura.id, factura);
  }

  private getIva(base: number, tipoIva: number): number {
    return base * tipoIva / 100;
  }

  private getTotal(base: number, iva: number): number {
    return base + iva;
  }

  private transformaFacturas(facturas: FacturaAPI[]): Factura[] {
    return facturas.map(
      factura => this.transformaFactura(factura)
    );
  }

  private transformaFactura(factura: FacturaAPI): Factura {
    return {
      ...factura,
      tipo: TiposFacturas[factura.tipo],
      fecha: moment(+factura.fecha),
      total: this.getTotal(factura.base, this.getIva(factura.base, factura.tipoIva)),
      iva: this.getIva(factura.base, factura.tipoIva),
      estado: factura.abonada ? EstadosFacturas.abonada : EstadosFacturas.pendiente
    };
  }

}
