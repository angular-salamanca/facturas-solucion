import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FacturasService } from 'src/app/facturas/facturas.service';
import { Factura } from 'src/app/facturas/model/factura.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'fact-cabecera',
  templateUrl: './cabecera.component.html',
  styleUrls: ['./cabecera.component.css']
})
export class CabeceraComponent implements OnInit, OnDestroy {

  @Input('miTitulo')
  public titulo = '';

  private suscripcion: Subscription;
  public facturas: Factura[];

  constructor(private facturasService: FacturasService) {
  }

  ngOnInit() {
    this.suscripcion = this.facturasService.facturas$.subscribe(
      facturas => {
        console.log(facturas);
        this.facturas = facturas;
      }
    );
  }

  ngOnDestroy() {
    this.suscripcion.unsubscribe();
  }
}
