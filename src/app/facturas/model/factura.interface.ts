import * as moment from 'moment';
import { TiposFacturas } from './tipos-facturas.enum';
import { EstadosFacturas } from './estados-facturas.enum';

export interface Factura {
  id: number;
  numero: string;
  fecha: moment.Moment;
  concepto: string;
  base: number;
  iva: number;
  total: number;
  tipoIva: number;
  tipo: TiposFacturas;
  abonada: boolean;
  estado: EstadosFacturas;
}
