import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CabeceraComponent } from './cabecera/cabecera.component';
import { SidebarComponent } from './sidebar/sidebar.component';

@NgModule({
  declarations: [CabeceraComponent, SidebarComponent],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports: [
    SidebarComponent,
    CabeceraComponent
  ]
})
export class CoreModule { }
