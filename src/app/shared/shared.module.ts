import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BuscadorComponent } from './buscador/buscador.component';
import { LoadingComponent } from './loading/loading.component';
import { EstadosFacturasPipe } from './estados-facturas.pipe';

@NgModule({
  declarations: [BuscadorComponent, LoadingComponent, EstadosFacturasPipe],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    BuscadorComponent,
    LoadingComponent,
    EstadosFacturasPipe
  ]
})
export class SharedModule { }
