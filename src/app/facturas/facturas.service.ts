import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Factura } from './model/factura.interface';
import { FacturaAPI } from './model/factura-api.interface';
import { TiposFacturas } from './model/tipos-facturas.enum';
import { EstadosFacturas } from './model/estados-facturas.enum';
import { BehaviorSubject } from 'rxjs';
import { ApiService } from '../core/api.service';

@Injectable({
  providedIn: 'root'
})
export class FacturasService {

  public facturas$ = new BehaviorSubject<Factura[]>([]);

  constructor(public api: ApiService) {
    this.api.getFacturas().subscribe(
      facturas => this.facturas$.next(facturas)
    );
  }

  public anyadirFactura() {
    const nuevaFactura = {
      id: null,
      numero: '9999',
      concepto: 'Nueva factura',
      base: 1000,
      iva: 121,
      total: 1121,
      tipoIva: 21,
      abonada: false,
      estado: EstadosFacturas.pendiente,
      tipo: TiposFacturas.gasto,
      fecha: moment()
    };
    const facturasAntiguas = this.facturas$.getValue();
    /*this.facturas$.next([
      ...facturasAntiguas,
      nuevaFactura
    ]);*/
    this.api.anyadirFactura(nuevaFactura).subscribe(
      factura => {
        nuevaFactura.id = factura.id;
        this.facturas$.next([
          ...this.facturas$.getValue(),
          nuevaFactura
        ]);
      }
    );
  }

  public borrarFactura(id: number) {
    this.api.borrarFactura(id).subscribe(
      () => {
        const facturasAntiguo = this.facturas$.getValue();
        const facturasNuevo = facturasAntiguo.filter(factura => factura.id !== id);
        this.facturas$.next(facturasNuevo);
      }
    );
  }

  public cambiarEstadoFactura(factura: Factura) {
    factura.abonada = !factura.abonada;
    factura.estado = factura.estado === EstadosFacturas.abonada ? EstadosFacturas.pendiente : EstadosFacturas.abonada;
    this.api.cambiarEstadoFactura(factura).subscribe(
      () => {
        const facturasAntiguo = this.facturas$.getValue();
        const facturasNuevo = facturasAntiguo.map(
          f => f.id === factura.id ? factura : f
        );
        this.facturas$.next(facturasNuevo);
      }
    );
  }

}
