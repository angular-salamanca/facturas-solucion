import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'fact-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css']
})
export class LoadingComponent implements OnInit, OnDestroy {

  private interval;

  constructor() { }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

}
